import requests
import json
import time
from bs4 import BeautifulSoup

i = 1

f = open("rg.txt", "w")
f.close()

dates = ["2013", "2014", "2015", "2017", "2018"]

match = 0

while True:

    print("PAGE : " + str(i))

    url = 'https://www.researchgate.net/search/publication?q=%2Bbreast%2Bcancer%&page=' + str(i)
    # print(url)
    page = requests.get(url, headers={
        'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
        'Cookie': '__cfduid=d527aa6ba3c616cde7e58f5e404dae9071576686646; sid=TdpFm4TH6zdl4MW1SBebBCBfT0qAB0030mHWJzKTg03F07AD3pkE1LUMLz0XZYqmT0dSWTXwGPANHN0uLbqg2Z6HWVV783Cgw9w0Ymi8YDUhKu4dX04QkheZJcmpY79O; did=XrpQvuz5myzTAPdLVGucMhEP7ytA4xMHzSE009DSNd6XSF80Hxama4GKYVbLGaSl; pl=deleted; ptc=RG1.5831091051963140357.1576686646'})

    i += 1

    soup = BeautifulSoup(page.text, 'html.parser')

    stopPattern = soup.find("div", {
        "class": "nova-e-text nova-e-text--size-m nova-e-text--family-sans-serif nova-e-text--spacing-none nova-e-text--color-inherit"})

    if stopPattern is not None:
        break

    articles = soup.find_all("div", {"class": "nova-c-card__body nova-c-card__body--spacing-inherit"})

    authorsList = []
    articlesList = []

    for article in articles:

        authorsList.clear()

        title = article.find("a", {"class": "nova-e-link nova-e-link--color-inherit nova-e-link--theme-bare"})

        date = article.find("li", {"class": "nova-e-list__item nova-v-publication-item__meta-data-item"}).text.strip()[-4:]

        if date in dates:
            match += 1
            print("NB MATCH : " + (str(match)))

            # f.write('-----------------\n')
            # f.write('Title : ' + title.get_text().strip() + '\n')

            authors = article.find_all("a", {"itemprop": "author"})

            for author in authors:
                authorsList.append(author.text)

            #print('Authors : ' + json.dumps(authorsList) + ']')

            # f.write('Authors : ' + json.dumps(authorsList) + '\n')

            js = {'title': title.text.strip(), 'date': date, 'authors': authorsList}
            #print(js)
            #articlesList.append(js)

            #print(json.dumps(js, indent=4))

            f = open('rg.txt', 'a')

            f.write(json.dumps(js) + '\n')

            f.close()
# time.sleep(1)

f.close()
