import networkx as nx
import math

G = nx.read_gexf("all.gexf")

initial_nb_connected_components = nx.number_connected_components(G)
current_nb_connected_components = initial_nb_connected_components

while current_nb_connected_components <= initial_nb_connected_components:


    betweenness = nx.edge_betweenness_centrality(G)

    highest_score = max(betweenness.values())

    for k,v in betweenness.items():
        if highest_score == float(v):
            G.remove_edge(k[0], k[1])
