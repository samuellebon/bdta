import requests
import json
from bs4 import BeautifulSoup

page = requests.get('https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=breast+cancer&terms-0-field=all&classification-physics_archives=all&classification-include_cross_list=include&date-year=&date-filter_by=date_range&date-from_date=2013&date-to_date=2018&date-date_type=submitted_date&abstracts=show&size=200&order=-announced_date_first')

soup = BeautifulSoup(page.text, 'html.parser')

articles = soup.find_all("li", {"class": "arxiv-result"})

authorsList = []
articlesList = []

f = open("arxiv.txt", "w")
f.close()

for article in articles:

    f = open('arxiv.txt', 'a')

    authorsList.clear()

    title = article.find("p", {"class": "title"})

    # print('-----------------')
    # print('Title : ' + title.get_text().strip())

    # f.write('-----------------\n')
    # f.write('Title : ' + title.get_text().strip() + '\n')

    authors = article.find("p", {"class": "authors"})

    for name in authors.find_all("a"):
        authorsList.append(name.text.strip())

    # print('Authors : ' + json.dumps(authorsList) + ']')

    # f.write('Authors : ' + json.dumps(authorsList) + '\n')

    js = {'title': title.text.strip(), 'authors': authorsList}
    articlesList.append(js)

    print(json.dumps(articlesList, indent=4))

    # f.write(json.dumps(articlesList) + '\n')

    f.close()
