import requests
import json
from bs4 import BeautifulSoup

page = requests.get('http://liinwww.ira.uka.de/csbib?query=%2bti:%22breast%20cancer%22%20%2byr:%5b2013%20TO%202019%5d&sort=score')

soup = BeautifulSoup(page.text, 'html.parser')

articles = soup.find_all("table", {"class": "citation"})

authorsList = []
articlesList = []

f = open("tcocsb.txt", "w")
f.close()

for article in articles:

    f = open('tcocsb.txt', 'a')

    authorsList.clear()

    title = article.find("span", {"class": "b_title"})

    print('-----------------')
    print('Title : ' + " ".join(title.get_text().strip().split()))

    # f.write('-----------------\n')
    # f.write('Title : ' + title.get_text().strip() + '\n')

    authors = article.find_all('a', title=lambda art_title: art_title and 'authored by' in art_title)

    for author in authors:
        authorsList.append(author.text.strip())

    print('Authors : ' + json.dumps(authorsList) + ']')

    # f.write('Authors : ' + json.dumps(authorsList) + '\n')

    # js = {'title': title.text.strip(), 'authors': authorsList}
    # articlesList.append(js)

    # print(json.dumps(articlesList, indent=4))

    # f.write(json.dumps(articlesList) + '\n')

    f.close()
