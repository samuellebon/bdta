import json
import networkx as nx
import collections
import matplotlib.pyplot as plt
import powerlaw

def save_graph(dic, filename):

    G = nx.Graph()
    G.add_nodes_from(dic.keys())

    for author, coauthors in dic.items():
        for coauthor in coauthors:
            G.add_edge(author, coauthor)

    nx.write_gexf(G, filename)
    return G

def graph_infos(G):

    print(nx.info(G))

    components = nx.connected_components(G)
    largest_component = max(components, key=len)

    subgraph = G.subgraph(largest_component)
    diameter = nx.diameter(subgraph)
    print("Network diameter of largest component : ", diameter)

    avg_shortest_path = nx.average_shortest_path_length(subgraph)
    print("Average shortest path of largest component : ", avg_shortest_path)

    nb_of_connected_components = nx.number_connected_components(G)
    print("Number of connected components : ", nb_of_connected_components)

    degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())

    fig, ax = plt.subplots()
    plt.bar(deg, cnt, width=0.80, color='b')

    plt.title("Degree Histogram")
    plt.ylabel("Count")
    plt.xlabel("Degree")
    ax.set_xticks([d + 0.4 for d in deg])
    ax.set_xticklabels(deg)

    # draw graph in inset
    plt.axes([0.4, 0.4, 0.5, 0.5])
    Gcc = G.subgraph(sorted(nx.connected_components(G), key=len, reverse=True)[0])
    pos = nx.spring_layout(G)
    plt.axis('off')
    nx.draw_networkx_nodes(G, pos, node_size=20)
    nx.draw_networkx_edges(G, pos, alpha=0.4)

    plt.show()

dic = {}
dic_2013 = {}
dic_2014 = {}
dic_2015 = {}
dic_2016 = {}
dic_2017 = {}
dic_2018 = {}
dic_all = {}

with open('articles.json') as json_file:
    data = json.load(json_file)

    for article in data:

        date = article["date"]
        authors = article["authors"]

        for author in authors:

            current_co_authors = list(filter(lambda x : x != author, authors))

            if author not in dic.keys():

                nested = {}
                nested[date] = current_co_authors

                dic[author] = nested
            else:

                nested = dic.get(author)
                coauthors = nested.get(date)

                if coauthors is None:
                    nested[date] = list(set(current_co_authors))
                else:
                    nested[date] = list(set(coauthors) | set(current_co_authors))

                dic[author] = nested

    # Sort by popularity

    popularity = {}

    for author, nested in dic.items():

        count = 0

        all_co_authors = []

        for year, co_authors in nested.items():
            all_co_authors = list(set(all_co_authors) | set(co_authors))
        
        popularity[author] = len(all_co_authors)

    popularity = dict(sorted(popularity.items(), key=lambda x: x[1], reverse=True))

    # Retrict number of nodes to average 10_000

    keys = list(popularity.keys())[0:1300]
    dic = {k:v for k,v in dic.items() if k in keys}

    # Fill dic by year

    for author, nested in dic.items():

        years = nested.keys()

        all_co_authors = []

        if '2013' in years:
            dic_2013[author] = nested['2013']
            all_co_authors = list(set(all_co_authors) | set(nested['2013']))

        if '2014' in years:
            dic_2014[author] = nested['2014']
            all_co_authors = list(set(all_co_authors) | set(nested['2014']))

        if '2015' in years:
            dic_2015[author] = nested['2015']
            all_co_authors = list(set(all_co_authors) | set(nested['2015']))

        if '2016' in years:
            dic_2016[author] = nested['2016']
            all_co_authors = list(set(all_co_authors) | set(nested['2016']))

        if '2017' in years:
            dic_2017[author] = nested['2017']
            all_co_authors = list(set(all_co_authors) | set(nested['2017']))

        if '2018' in years:
            dic_2018[author] = nested['2018']
            all_co_authors = list(set(all_co_authors) | set(nested['2018']))

        dic_all[author] = all_co_authors
    
    # Print graph infos

    G = save_graph(dic_2013, "2013.gexf")
    # graph_infos(G)

    G = save_graph(dic_2014, "2014.gexf")
    # graph_infos(G)

    G = save_graph(dic_2015, "2015.gexf")
    # graph_infos(G)

    G = save_graph(dic_2016, "2016.gexf")
    # graph_infos(G)

    G = save_graph(dic_2017, "2017.gexf")
    # graph_infos(G)

    G = save_graph(dic_2018, "2018.gexf")
    # graph_infos(G)

    G = save_graph(dic_all, "all.gexf")

    # degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    # print(degree_sequence)

    # print(str(len(degree_sequence)))

    # results = powerlaw.Fit(degree_sequence, verbose=False, discrete=True, xmin=19)
    # print('Xmin : ' +  str(results.xmin))
    # print('Alpha : ' + str(results.alpha))
    # R, p = results.distribution_compare('power_law', 'lognormal')
    # print(R, p)

    # R, p = results.distribution_compare('power_law', 'lognormal')
    # print('lognormal')
    # print(R, p)
    # R, p = results.distribution_compare('power_law', 'exponential')
    # print('exponential')
    #print(R, p)
    # R, p = results.distribution_compare('power_law', 'lognormal_positive')
    # print('lognormal positive')
    # print(R, p)
    # R, p = results.distribution_compare('power_law', 'stretched_exponential')
    # print('stretched exponential')
    # print(R, p)
    # R, p = results.distribution_compare('power_law', 'truncated_power_law')
    # print('truncated powerlaw')
    # print(R, p)


    # graph_infos(G)
