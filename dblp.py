import requests
import json
from bs4 import BeautifulSoup

# Only 2013 results
page = requests.get('https://dblp.uni-trier.de/search/publ/inc?q=year%3A2013%3A%20breast%20cancer&h=1000')

soup = BeautifulSoup(page.text, 'html.parser')

articles = soup.find_all("cite", {"class": "data"})

authorsList = []
articlesList = []

f = open("dblp.txt", "w")
f.close()

for article in articles:
    f = open('dblp.txt', 'a')

    authorsList.clear()

    title = article.find("span", {"class": "title"})

    print('-----------------')
    print('Title : ' + title.get_text().strip())

    # f.write('-----------------\n')
    # f.write('Title : ' + title.get_text().strip() + '\n')

    authors = article.find_all("span", {"itemprop": "author"})

    for author in authors:
        authorsList.append(author.find("a").text.strip())

    print('Authors : ' + json.dumps(authorsList) + ']')

    # f.write('Authors : ' + json.dumps(authorsList) + '\n')

    js = {'title': title.text.strip(), 'authors': authorsList}

    # articlesList.append(js)

    # print(json.dumps(articlesList, indent=4))

    # f.write(json.dumps(articlesList) + '\n')

    f.close()
