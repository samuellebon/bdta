import requests
import json
import time
from bs4 import BeautifulSoup

fileName = "wos.txt"

i = 1

f = open(fileName, "w")
f.close()

dates = ["2013", "2014", "2015", "2017", "2018"]

match = 0
homonym = 0

dic = {}

while i == 1:

    print("PAGE : " + str(i))

    url = 'http://apps.webofknowledge.com.ezproxy.u-pec.fr/summary.do?product=WOS&parentProduct=WOS&search_mode=AdvancedSearch&qid=6&SID=E1GYBdgjJTzwPSHSPGH&&action=sort&sortBy=LC.D;PY.D;AU.A.en;SO.A.en;VL.D;PG.A&showFirstPage=' + str(i)+ '&isCRHidden=false&page=' + str(i)
    page = requests.get(url, headers={
        'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
        'Cookie': '_sp_id.abc9=9c9817bb-22ea-4cc2-93b8-98393f78569e.1576765518.3.1576787784.1576770929.9cb87163-b2a0-4854-9edc-3d524bc304d4; _hjMinimizedPolls=465224; ezproxy=IrsvtlSEAGGh07q; _hjid=5faa08e3-cd3a-448f-a2b8-d7178c3dbf04'})

    print(page.status_code)

    i += 1

    soup = BeautifulSoup(page.text, 'html.parser')

    articles = soup.find_all("div", {"class": "search-results-item"})

    authorsList = []
    articlesList = []

    for article in articles:

        authorsList.clear()

        title = article.find("a", {"class": "smallV110 snowplow-full-record"})

        date = article.find_all("span", {"class": "data_bold"})[-1].text.strip()[-4:]

        authors = article.find_all("a", {"title": "Find more records by this author"})

        for author in authors:

            name = author.text.replace(',', '')
            dai = author["href"].split("daisIds=",1)[1]

            if dai in dic:

                homonym += 1

                print("<!> Homonym found <!> : TOTAL = " + str(homonym))

                currentName = dic.get(dai)
                print("old : " + currentName)
                print("new : " + name)

                # Replace author name by the most complete in the dic
                if len(name) > len(currentName):

                    dic[dai] = name

                    with open(fileName, 'w') as f:
                        s = f.read()
                        print('<!> Replacing ' + currentName + 'by : ' + name + ' <!>') 
                        s = s.replace(currentName, name)
                        f.write(s)

                else:
                    name = currentName

            else:
                dic[dai] = name

            authorsList.append(name)

        js = {'title': title.text.strip(), 'date': date, 'authors': authorsList}

        with open(fileName, 'a') as f:
            f.write(json.dumps(js) + '\n')

f.close()